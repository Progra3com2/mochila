package mochila;

public class Objeto {
	
	private String _nombre;
	private int _peso;
	private int _beneficio;
	
	public Objeto(String nombre, int peso, int beneficio) {
		_nombre = nombre;
		_peso = peso;
		_beneficio = beneficio;
	}

	public String get_nombre() {
		return _nombre;
	}

	public int get_peso() {
		return _peso;
	}

	public int get_beneficio() {
		return _beneficio;
	}

	public double cociente() {
		return get_beneficio() / (double) get_peso();
	}
	
}
