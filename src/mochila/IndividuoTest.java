package mochila;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class IndividuoTest {

	Instancia _instancia;
	
	@Before
	public void inicializar() {
		_instancia = new Instancia(10);
		_instancia.agregarObjeto(new Objeto("Agua",2,3));
		_instancia.agregarObjeto(new Objeto("Botas",6,10));
		_instancia.agregarObjeto(new Objeto("Calentador",3,8));
		_instancia.agregarObjeto(new Objeto("Carpa",4,9));
		_instancia.agregarObjeto(new Objeto("Lena",5,11));
		_instancia.agregarObjeto(new Objeto("Linterna",1,4));
		_instancia.agregarObjeto(new Objeto("Toalla",1,2));
	}
	
	@Test
	public void pesoVacioTest() {
		Individuo individuo = crearIndividuo("0000000");
		assertEquals(0.0, individuo.peso(), 0.001);
	}

	@Test
	public void pesoCompletoTest() {
		Individuo individuo = crearIndividuo("1111111");
		assertEquals(22.0, individuo.peso(), 0.001);
	}

	@Test
	public void pesoTest() {
		Individuo individuo = crearIndividuo("0100010");
		assertEquals(7.0, individuo.peso(), 0.001);
	}

	@Test
	public void beneficioVacioTest() {
		Individuo individuo = crearIndividuo("0000000");
		assertEquals(0.0, individuo.beneficio(), 0.001);
	}

	@Test
	public void beneficioCompletoTest() {
		Individuo individuo = crearIndividuo("1111111");
		assertEquals(47.0, individuo.beneficio(), 0.001);
	}

	@Test
	public void beneficioTest() {
		Individuo individuo = crearIndividuo("0100010");
		assertEquals(14.0, individuo.beneficio(), 0.001);
	}
	
	@Test
	public void mutarPrimeroTest() {
		Individuo individuo = crearIndividuo("0100010");
		mutar(individuo,0);
		assertIndividuo("1100010",individuo);
	}

	@Test
	public void mutarUltimoTest() {
		Individuo individuo = crearIndividuo("0100011");
		mutar(individuo,6);
		assertIndividuo("0100010",individuo);
	}

	@Test
	public void mutarCerosTest() {
		Individuo individuo = crearIndividuo("0000000");
		mutar(individuo,5);
		assertIndividuo("0000010",individuo);
	}

	@Test
	public void mutarTest() {
		Individuo individuo = crearIndividuo("0100010");
		mutar(individuo,3);
		assertIndividuo("0101010",individuo);
	}

	@Test
	public void recombinarAlInicioTest() {
		Individuo individuo1 = crearIndividuo("1111111");
		Individuo individuo2 = crearIndividuo("0000000");
		Individuo[] hijos = recombinar(individuo1,individuo2,0);
		assertIndividuo("0000000",hijos[0]);
		assertIndividuo("1111111",hijos[1]);
	}

	@Test
	public void recombinarAlFinalTest() {
		Individuo individuo1 = crearIndividuo("1111111");
		Individuo individuo2 = crearIndividuo("0000000");
		Individuo[] hijos = recombinar(individuo1,individuo2,6);
		assertIndividuo("1111110",hijos[0]);
		assertIndividuo("0000001",hijos[1]);
	}

	@Test
	public void recombinarTest() {
		Individuo individuo1 = crearIndividuo("1111111");
		Individuo individuo2 = crearIndividuo("0000000");
		Individuo[] hijos = recombinar(individuo1,individuo2,3);
		assertIndividuo("1110000",hijos[0]);
		assertIndividuo("0001111",hijos[1]);
	}

	@Test
	public void fitnessTest() {
		Individuo individuo = crearIndividuo("0100010");
		assertEquals(14.0, individuo.fitness(), 0.001);
	}

	@Test
	public void fitnessExcedidoTest() {
		Individuo individuo = crearIndividuo("1111111");
		assertEquals(-12.0, individuo.fitness(), 0.001);
	}

	@Test
	public void fitnessPesoJustoTest() {
		Individuo individuo = crearIndividuo("1100011");
		assertEquals(19.0, individuo.fitness(), 0.001);
	}

	@Test
	public void fitnessPesoExcedPorUnoTest() {
		Individuo individuo = crearIndividuo("1110000");
		assertEquals(-1.0, individuo.fitness(), 0.001);
	}


	private void assertIndividuo(String str, Individuo individuo) {
		for (int i=0; i<str.length(); i++)
			assertEquals(str.charAt(i)=='1',individuo.get(i));
	}

	private Individuo crearIndividuo(String str) {
		Individuo.setGenerador(new GeneradorPrefijado(str));
		Individuo individuo = Individuo.aleatorio(_instancia);
		return individuo;
	}

	private void mutar(Individuo individuo, int bit) {
		Individuo.setGenerador(new GeneradorPrefijado(bit));
		individuo.mutar();
	}

	private Individuo[] recombinar(Individuo individuo1, Individuo individuo2, int bit) {
		Individuo.setGenerador(new GeneradorPrefijado(bit));
		return individuo1.recombinar(individuo2);
	}

}
