package mochila;

import java.util.ArrayList;

public class Instancia {
	
	private int _capacidad;
	private ArrayList<Objeto> _objetos;
	
	public Instancia(int capacidad) {
		_capacidad = capacidad;
		_objetos = new ArrayList<Objeto>();
	}
	
	public void agregarObjeto(Objeto obj) {
		_objetos.add(obj);
	}

	public int get_capacidad() {
		return _capacidad;
	}

	public ArrayList<Objeto> get_objetos() {
		ArrayList<Objeto> ret = new ArrayList<Objeto>();
		for (Objeto o: _objetos)
			ret.add(o);
		return ret;
	}

	public Objeto getObjeto(int i) {
		return _objetos.get(i);
	}
	
	public int get_tamano() {
		return _objetos.size();
	}

}
