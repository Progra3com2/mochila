package mochila;

import java.util.Comparator;

public class ComparadorPorPeso implements Comparator<Objeto>
{

	@Override
	public int compare(Objeto uno, Objeto otro) {
		return uno.get_peso() - otro.get_peso();
	}

}
