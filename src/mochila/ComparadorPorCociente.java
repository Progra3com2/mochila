package mochila;

import java.util.Comparator;

public class ComparadorPorCociente implements Comparator<Objeto>{

	@Override
	public int compare(Objeto uno, Objeto otro) {
		double dif = otro.cociente() - uno.cociente();
		if (dif<0)
			return -1;
		else if (dif==0)
			return 0;
		else
			return 1;
	}

}
