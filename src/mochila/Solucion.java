package mochila;

import java.util.ArrayList;

public class Solucion {

	private ArrayList<Objeto> _objetos;
	private int _peso;
	private int _beneficio;
	
	public Solucion() {
		_objetos = new ArrayList<Objeto>();
	}
	
	public void agregarObjeto(Objeto obj) {
		_objetos.add(obj);
		_peso += obj.get_peso();
		_beneficio += obj.get_beneficio();
	}

	public int cardinal() {
		return _objetos.size();
	}

	public int get_peso() {
		return _peso;
	}

	public int get_beneficio() {
		return _beneficio;
	}
	

}
