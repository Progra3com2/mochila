package mochila;

import java.util.Comparator;

public class ComparadorPorBeneficio implements Comparator<Objeto> {

	@Override
	public int compare(Objeto uno, Objeto otro) {
		return otro.get_beneficio() - uno.get_beneficio();
	}

}
