package mochila;

import static org.junit.Assert.*;

import java.util.Comparator;

import org.junit.Test;

public class SolverGolosoTest {

	@Test
	public void porPesoTest() {
		SolverGoloso solver = new SolverGoloso(ejemplo(),new Comparator<Objeto>() {
				@Override
				public int compare(Objeto uno, Objeto otro) {
					return uno.get_peso() - otro.get_peso();
				}
			});
		Solucion sol = solver.resolver();

		assertEquals(4,sol.cardinal());
		assertEquals(7,sol.get_peso());
		assertEquals(17,sol.get_beneficio());
	}

	@Test
	public void porBeneficioTest() {
		SolverGoloso solver = new SolverGoloso(ejemplo(),new Comparator<Objeto>() {
				@Override
				public int compare(Objeto uno, Objeto otro) {
					return otro.get_beneficio() - uno.get_beneficio();
				}
			});
		Solucion sol = solver.resolver();

		assertEquals(3,sol.cardinal());
		assertEquals(10,sol.get_peso());
		assertEquals(24,sol.get_beneficio());
	}

	@Test
	public void porCocienteTest() {
		SolverGoloso solver = new SolverGoloso(ejemplo(),new Comparator<Objeto>() {
				@Override
				public int compare(Objeto uno, Objeto otro) {
					double dif = otro.cociente() - uno.cociente();
					if (dif<0)
						return -1;
					else if (dif==0)
						return 0;
					else
						return 1;
				}
			});
		Solucion sol = solver.resolver();

		assertEquals(4,sol.cardinal());
		assertEquals(9,sol.get_peso());
		assertEquals(23,sol.get_beneficio());
	}

	private Instancia ejemplo() {
		Instancia ret = new Instancia(10);
		ret.agregarObjeto(new Objeto("Agua",2,3));
		ret.agregarObjeto(new Objeto("Botas",6,10));
		ret.agregarObjeto(new Objeto("Calentador",3,8));
		ret.agregarObjeto(new Objeto("Carpa",4,9));
		ret.agregarObjeto(new Objeto("Lena",5,11));
		ret.agregarObjeto(new Objeto("Linterna",1,4));
		ret.agregarObjeto(new Objeto("Toalla",1,2));
		return ret;
	}
}
