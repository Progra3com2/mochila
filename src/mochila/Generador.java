package mochila;

public interface Generador {
	boolean nextBoolean();
	int nextInt(int rango);
}
