package mochila;

import java.util.ArrayList;
import java.util.Collections;

public class Poblacion {
	
	// Los individuos de la poblacion
	private ArrayList<Individuo> _individuos;
	
	// Instancia asociada
	private Instancia _instancia;
	
	// Generador de numeros aleatorios
	private Generador _random;

	// Observadores registrados
	private ArrayList<Observador> _observadores;
	
	// Parametros de la poblacion
	private int _tamano = 100;
	private int _mutadosPorIteracion = 10;
	private int _recombinadosPorIteracion = 20;
	private int _eliminadosPorIteracion = 60;
	private int _maxIteraciones = 1000;
	
	// Estadisticas
	private int _iteracion;
	
	public Poblacion(Instancia instancia, Generador generador) {
		_instancia = instancia;
		_random = generador;
		_observadores = new ArrayList<Observador>();
	}
	
	public void registrar(Observador observador) {
		_observadores.add(observador);
	}

	public void simular() {
		generarIndividuos();
		_iteracion = 0;
		
		while (!satisfactoria()) {
			mutarAlgunos();
			recombinarAlgunos();
			eliminarPeores();
			agregarNuevos();

			_iteracion++;
			notificarObservadores();
		}
	}

	private void notificarObservadores() {
		for (Observador observador: _observadores)
			observador.notificar();
	}

	private void generarIndividuos() {
		_individuos = new ArrayList<Individuo>(_tamano);
		Individuo.setGenerador(new GeneradorAleatorio());
		for (int i=0; i<_tamano; i++)
			_individuos.add(Individuo.aleatorio(_instancia));
	}

	private boolean satisfactoria() {
		return _iteracion == _maxIteraciones;
	}

	private void mutarAlgunos() {
		for (int j=0; j<_mutadosPorIteracion; j++) {
			individuoAlAzar().mutar();
		}
	}

	private void recombinarAlgunos() {
		for (int j=0; j<_recombinadosPorIteracion; j++) {
			Individuo padre1 = individuoAlAzar();
			Individuo padre2 = individuoAlAzar();

			for (Individuo hijo: padre1.recombinar(padre2))
				_individuos.add(hijo);
		}
	}

	private void eliminarPeores() {
		Collections.sort(_individuos);
		Collections.reverse(_individuos); // Para que cada remove sea O(1)
		
		for (int i=0; i<_eliminadosPorIteracion; i++)
			_individuos.remove(_individuos.size()-1);
	}

	private void agregarNuevos() {
		while (_individuos.size() < _tamano)
			_individuos.add(Individuo.aleatorio(_instancia));
	}

	public Individuo mejorIndividuo() {
		return Collections.max(_individuos);
	}
	
	public Individuo peorIndividuo() {
		return Collections.min(_individuos);
	}

	public double fitnessPromedio() {
		double suma = 0;
		for (Individuo individuo: _individuos)
			suma += individuo.fitness();
		return suma / _individuos.size();
	}

	private Individuo individuoAlAzar() {
		int i = _random.nextInt(_individuos.size());
		return _individuos.get(i);
	}

	public int getIteracion() {
		return _iteracion;
	}

}
