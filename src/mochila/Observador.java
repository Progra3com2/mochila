package mochila;

public interface Observador {
	void notificar();
}
