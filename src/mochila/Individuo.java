package mochila;

public class Individuo implements Comparable<Individuo> {
	// Representamos a un invididuo como una secuencia de bits
	// 0=el objeto no esta / 1=el objeta esta
	private boolean[] _bits;
	
	// Instancia asociada
	private Instancia _instancia;
	
	// Generador de valores aleatorios
	private static Generador _random;

	public static void setGenerador(Generador generador) {
		_random = generador;
	}

	public static Individuo aleatorio(Instancia instancia) {
		Individuo ret = new Individuo(instancia);

		for (int i=0; i<instancia.get_tamano(); i++)
			ret.set(i, _random.nextBoolean());
		
		return ret;
	}

	private Individuo(Instancia instancia) {
		_instancia = instancia;
		_bits = new boolean[instancia.get_tamano()];
	}

	public void mutar() {
		// El bit que se muta se decide aleatoriamente
		int k = _random.nextInt(_bits.length);
		set(k, !get(k));
	}

	public Individuo[] recombinar(Individuo other) {
		// El punto de recombinacion se decide aleatoriamente
		int k = _random.nextInt(_bits.length);
		
		Individuo hijo1 = new Individuo(_instancia);
		Individuo hijo2 = new Individuo(_instancia);

		for (int i=0; i<k; i++) {
			hijo1.set(i, this.get(i));
			hijo2.set(i, other.get(i));
		}
		for (int i=k; i<_bits.length; i++) {
			hijo1.set(i, other.get(i));
			hijo2.set(i, this.get(i));
		}

		return new Individuo[] {hijo1, hijo2};
	}

	public double fitness() {
		if (peso() > _instancia.get_capacidad())
			return _instancia.get_capacidad() - peso();
		else
			return beneficio();
	}
	
	public double beneficio() {
		double ret = 0.0;
		for (int i=0; i<_instancia.get_tamano(); i++) {
			if (get(i))
				ret += _instancia.getObjeto(i).get_beneficio();
		}
		return ret;
	}
	
	public double peso() {
		double ret = 0.0;
		for (int i=0; i<_instancia.get_tamano(); i++) {
			if (get(i))
				ret += _instancia.getObjeto(i).get_peso();
		}
		return ret;
	}
	
	boolean get(int i) {
		return _bits[i];
	}
	private void set(int i, boolean valor) {
		_bits[i] = valor;
	}

	@Override
	public int compareTo(Individuo other) {
		if (this.fitness() < other.fitness())
			return -1;
		else if (this.fitness() == other.fitness())
			return 0;
		else
			return 1;
	}
	
}
