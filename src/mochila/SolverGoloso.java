package mochila;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class SolverGoloso {

	private Instancia _instancia;
	private Comparator<Objeto> _comparador;
	
	public SolverGoloso(Instancia instancia, Comparator<Objeto> comparador) {
		_instancia = instancia;
		_comparador = comparador;
	}
	
	public Solucion resolver() {
		Solucion ret = new Solucion();
		
		for (Objeto obj: objetosOrdenados()) {
			if (ret.get_peso()+obj.get_peso()<=_instancia.get_capacidad())
				ret.agregarObjeto(obj);
		}

		return ret;
	}

	private ArrayList<Objeto> objetosOrdenados() {
		ArrayList<Objeto> ret = _instancia.get_objetos();
		Collections.sort(ret,_comparador);;
		return ret;
	}
	
}
